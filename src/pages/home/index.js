import React, { useEffect } from "react";
import { shape } from "prop-types";
import cookie from "react-cookies";

import RefProvider from "Utilities/refProvider";
import RefErrorBoundary from "Utilities/refErrorBoundary";
import { formStoreData } from "Utilities/helpers";
import Homepage from "./homePage";

const Home = (props) => {
  const propShape = formStoreData(props, ["dashboard"]);

  useEffect(() => {
    cookie.remove("access_token");
  }, []);

  return (
    <>
      <RefProvider data={propShape}>
        <RefErrorBoundary {...props}>
          <Homepage />
        </RefErrorBoundary>
      </RefProvider>
    </>
  );
};

Home.propTypes = {
  store: shape({}).isRequired,
  actions: shape({}).isRequired,
  location: shape({}).isRequired,
  history: shape({}).isRequired,
};

export default Home;
